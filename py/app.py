# Mutual Donation exploration
# Florian Habermacher
# 20210619
import shutil

import flib
from flib import *
from ftriv import *
import pandas as pd
from pandas.api.types import is_numeric_dtype
import statsmodels
import statsmodels.api as sm
#from pytictoc import TicToc
import argparse
from scipy import stats, interpolate
from pandas.api.types import is_numeric_dtype
#import psycopg2
from operator import itemgetter
from typing import Callable, TypeVar, Iterable, Tuple, Union, List, Dict
import traceback
from threading import Lock
from collections import defaultdict
import copy
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
# # To treat all warnings as kind of errors: make them crash & also catchable using 'try' & 'except RuntimeWarning:' :
# #   From https://stackoverflow.com/a/30368735/3673329
# import warnings
# warnings.filterwarnings("error")
import os
#import readchar


def readraw(fn, index_col=None):
    # Need to read with openpyxl now, see e.g.
    #   https://stackoverflow.com/questions/48066517/python-pandas-pd-read-excel-giving-importerror-install-xlrd-0-9-0-for-excel
    df = pd.read_excel(fn, engine='openpyxl', index_col=index_col)
    return df

def init_df(df):
    for m in modes:
        if 'stage_'+m not in df.columns: df['stage_'+m]=np.nan
        df['stage_'+m] = df['stage_'+m].fillna(0)
        df['stage_' + m] = df['stage_' + m].astype(int)

        if 'date_'+m not in df.columns: df['date_'+m]=np.nan
        df['date_'+m] = df['date_'+m].fillna(now)

        if 'failed_'+m not in df.columns: df['failed_'+m]=False
        if 'redo_'+m not in df.columns: df['redo_'+m]=False

def clean_df(df):
    dropbases = ['failed_', 'redo_', 'todo_']
    delcol = flatlist([[base+m for m in modes] for base in dropbases])
    try:
        df.drop(delcol,inplace=True,axis=1)
    except Exception as e:
        fd(f'Failed to drop columns: {e.message}, {e.args}')
        fd(f'COLS ARE {df.columns}\nTRIED REMOVALS WERE {delcol}')

def getstages(fn):
    # Returns function that maps stage -> days
    df_stages = readraw(stagefn, index_col='stage')
    dic_stages = {}
    for idx,row in df_stages.iterrows():
        dic_stages[idx]=row['days']
    stagedays = lambda stage: dic_stages[stage]
    nstage = len(df_stages)
    return stagedays, nstage

def flag_todo(df, stagedays, disp = 2, rounddays=True, modeuse=None):
    if modeuse==None: modeuse = list(modes)
    # Note, ( x / np.timedelta64(1, 'D') should convert the timedelta format into days (hopefully),
    #   See https://stackoverflow.com/questions/26456825/convert-timedelta64ns-column-to-seconds-in-python-pandas-dataframe/50500560
    if disp >= 2:
        fd(f'List of {len(df)} Entries')
    for m in modeuse:
        days_since_last = (now - df['date_'+m])/np.timedelta64(1, 'D')
        # fdtmp('hoh ',days_since_last)
        if rounddays:
            days_since_last = np.round(days_since_last)
        df['todo_'+m] = np.where(days_since_last >= df['stage_'+m].apply(stagedays), True, False)
        if disp>=2:
            print(f'For {modes[m]["name"]}: Todo {df["todo_"+m].sum()}')
        # fdtmp("Heya: ", np.where(days_since_last >= df['stage_'+m].apply(stagedays), True, False))
        if disp>=2:
            for s in range(nstage):
                n = (df['todo_'+m] & (df['stage_'+m]==s)).sum()
                if n:
                    print(f'    Stage {s:2}: {n:4}     ({stagedays(s):4} days)')

def randomize_todo(df, modeuse=None):
    if modeuse==None: modeuse = list(modes)
    todo = {}
    for m in modeuse:
        todo_ = np.where(df['todo_'+m]==True)[0]
        # todo_ = df[df['todo_'+m]==True]
        n = len(todo_)
        idx = np.random.choice(n, replace=False, size=n)
        todo[m] = todo_[idx]
        # todo[m] = todo_.iloc[idx]
    return todo


# From https://qastack.com.de/programming/510357/python-read-a-single-character-from-the-user :

class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()

# Alternative based on [=reading key]
#   https://qastack.com.de/programming/510357/python-read-a-single-character-from-the-user :
# And [=converting key from byte to str]
#   https://stackoverflow.com/questions/606191/convert-bytes-to-a-string
def getChar(decode='utf-8'):
    answer = None
    try:
        # for Windows-based systems
        import msvcrt # If successful, we are on Windows
        answer = msvcrt.getch()
        # return msvcrt.getch()

    except ImportError:
        # for POSIX-based systems (with termios & tty support)
        import tty, sys, termios  # raises ImportError if unsupported

        fd = sys.stdin.fileno()
        oldSettings = termios.tcgetattr(fd)

        try:
            tty.setcbreak(fd)
            answer = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, oldSettings)

    if decode==None:
        return answer
    else:
        return answer.decode(decode)




if sys.platform == 'win32':
    import msvcrt
    getch = msvcrt.getch
    getche = msvcrt.getche
else:
    import sys
    import termios
    def __gen_ch_getter(echo):
        def __fun():
            fd = sys.stdin.fileno()
            oldattr = termios.tcgetattr(fd)
            newattr = oldattr[:]
            try:
                if echo:
                    # disable ctrl character printing, otherwise, backspace will be printed as "^?"
                    lflag = ~(termios.ICANON | termios.ECHOCTL)
                else:
                    lflag = ~(termios.ICANON | termios.ECHO)
                newattr[3] &= lflag
                termios.tcsetattr(fd, termios.TCSADRAIN, newattr)
                ch = sys.stdin.read(1)
                if echo and ord(ch) == 127: # backspace
                    # emulate backspace erasing
                    # https://stackoverflow.com/a/47962872/404271
                    sys.stdout.write('\b \b')
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, oldattr)
            return ch
        return __fun
    getch = __gen_ch_getter(False)
    getche = __gen_ch_getter(True)

def backup(fn, disp=0):
    if ispathonly(fn):
        return False
    fndir = os.path.dirname(fn)
    backupdir = fndir+'\\backup\\'
    mkdirIfNotExist(backupdir)
    targetfn = backupdir+pathlib.Path(fn).stem+timestamp()+pathlib.Path(fn).suffix
    ok = True
    try:
        if True:
            shutil.copy2(fn,targetfn)
        else:
            shutil.copyfile(fn,targetfn)
        if disp:
            fd(f'Copied {fn} to {targetfn}')
    except Exception as e:
    # except e as error:
        ok = False
    nbackup = len(filesinpath(backupdir))
    nbackupfilecomplain = 200
    if nbackup>nbackupfilecomplain:
        fd(f"MIND: MORE THAN {nbackupfilecomplain} backups; you should pbly remove the oldest ones!")
    return ok

def delete_last_line(sameline=False):
    # Based on https://stackoverflow.com/questions/19596750/is-there-a-way-to-clear-your-printed-text-in-python
    "Use this function to delete the last line in the STDOUT"

    #cursor up one line
    if not sameline:
        sys.stdout.write('\x1b[1A')

    #delete last line
    sys.stdout.write('\x1b[2K')

#
# ########## FOR DEMO ################
# if __name__ == "__main__":
#     print("hello")
#     print("this line will be delete after 2 seconds")
#     time.sleep(2)
#     delete_last_line()
# ####################################

if __name__ == '__main__':
    os.system('chcp 936') # Enable printing Chinese Characters on Win console, see https://stackoverflow.com/a/53631316/3673329
    fn = "D:\Dropbox\Privat\Chinese\learninglist.xlsx"
    stagefn = "D:\Dropbox\Privat\Chinese\stages.xlsx"
    now = datetime.datetime.now()

    modes = {'ep': {'name':'en -> pinyin', 'prompt':'english','search':['pinyin','chinese']},
             'pe': {'name':'pinyin -> en', 'prompt':'pinyin','search':['english','chinese']},
             'ph': {'name':'chinese -> pinyin & en', 'prompt':'chinese','search':['english','pinyin']},
             }
    freqshowcontrol = [None, 10, 20, 50][2]  # After how many items remind user of controls
    df = readraw(fn)
    init_df(df)

    stagedays, nstage = getstages(stagefn)

    flag_todo(df, stagedays, disp=2)

    todo = randomize_todo(df)

    def readcharacter(msg='', print=False, newline=False):
        fdsl(msg)
        a = getChar()
        if print:
            fdsl(a)
        if newline:
            fd()
        return a

    # Chose modes to train
    def modesinfo(modes_):
        return ',     '.join([f'{m}: {mode["name"]}' for m,mode in modes.items()])
    fd(f'\nChoose modes: {modesinfo(modes)}')
    modeuse = modes.copy()
    ok=False
    while not ok:
        modenames = ''
        for m in modes:
            if 'x' in m:
                ferr("oops, expected to not have any x in modenames. must adapt continuation token please")
            modenames = modenames + (m if m in modeuse else ' '*len(m)) + '  '
        delete_last_line(True)
        fdsl(f'\r  {modenames}    Add/remove any (options {list(modes)}; x to continue): ')
        k = ''
        subok=False
        while not (subok or ok):
            k = k + readcharacter(print=True, newline=False)
            if 'x' in k:
                ok=True
            elif k in modes:
                if k in modeuse:
                    modeuse.pop(k)
                else:
                    modeuse[k]=modes[k].copy()
                subok=True
            elif len(k)>=max([len(mm) for mm in modes]):
                fd(f" Unrecognized mode, please choose among {list(modes)}")
                k=''
                subok=True

    fd(f'\n\nTRAINING {modesinfo(modeuse)}\n')

    # Training
    # _ wait it is rather any key that means "show", no? But should explain this to user
    def showcontrols():
        fd('Controls: y (or space)=good, n=not good, r=not good: ask again but keep flagged as failed, k: correct it: as y, but ensure NOT flagged as failed, x=abort, u=up,d=down\n'
           'Before solution shown: space to show solution, or y, k etc. to directly jump to conclusion.')
        # fd('Controls: y (or space)=good, n=not good, r=not good: ask again but keep flagged as failed, k: correct it: as y, but ensure NOT flagged as failed, x=abort, s = show, u=up,d=down')
    showcontrols()

    first = True
    todocount = lambda: sum([len(todo[m]) for m in modeuse])
    # todocount = sum([todo[m]['todo_' + m].sum() for m in modeuse])
    alldone = todocount()==0 #sum([len(todo[m]) for m in modes])
    redocount = lambda: sum([df.loc[todo[m],'redo_'+m].sum() for m in modeuse])
    # redocount = lambda: sum([df.loc[todo[m],:]['redo_' + m].sum() for m in modeuse])
    # redocount = lambda: sum([todo[m]['redo_' + m].sum() for m in modeuse])
    k=''
    counter = 0
    donesomething_ = False  # Whether at all changed anything. If not, then no need to save
    def donesomething():  # Confirm that have donesomething
        donesomething_ = True
    while not alldone:
        if first:
            fd(f'Doing {todocount()}')
        else:
            for m in modeuse:
                # todo_ = todo[m]
                todo[m] = np.where(df['redo_'+m])[0]
                # todo_ = np.where(df['redo_'+m])[0]
            fd(f'Repeating those that are to be redone (they remain at stage 0 anyways though): {redocount()} (={todocount()} ? should, I think)')

        for m,mname in modeuse.items():
            print(f'\nMode {mname}:\n')
            todo_ = todo[m]
            k=''
            #___ wait change everything: work only with df (not rowcopy & todo), as there are slices/copy/not copy problems with todo etc.; todo to only be indexes, e.g. list of indexes, basta.
            #for i,row in todo_.iterrows():

            i=0
            imaxsofar=0 # Used to avoid going, with 'd', further down than we've alredy been
            while i<len(todo_): # Now manual loop instead of for, to readily allow manual changes in index, to do corrections
                imaxsofar=max(imaxsofar,i)
                idx=todo_[i]
            # for i,idx in enumerate(todo_):

            # for i in range(len(todo_)):
                # fd(f"\n\nwell {todo_.iloc[i] is row}\n\n")

                ####### COPY df -> rowcopy ##############
                # Note we make explicitly a copy, as else iloc views/slices etc. extremely dangerous/unpredictable.
                # See also e.g. good explanation in explanation pbly in https://www.practicaldatascience.org/html/views_and_copies_in_pandas.html
                rowcopy = df.iloc[idx].copy() # Mind this is a copy! So, in the end after changing rowcopy, must copy that data back into df
                #########################################

                # rowcopy = todo_.iloc[i] # Mind is typically a copy, unless all data are same type/objects, afaik!
                # fd(todo_.iloc[i])
                # fd(rowcopy)

                # fd(f"\n\nwell {todo_.iloc[i] == row} {todo_.iloc[i] is row}\n\n")
                prompt = modes[m]['prompt']
                search = modes[m]['search']
                revising = False # Only
                k = readcharacter(msg=f'{rowcopy[prompt]:12} (s {rowcopy["stage_" + m]:2}) ')  # input()  # os.system("pause")
                if k == 'e':
                    break
                for s in search:
                    fdsl(f'{rowcopy[s]:12}')
                if k in ['y','n','r','k','s','u','d']:
                    pass  # Take as indication that already knew this element is good
                else:
                    k = readcharacter(msg='Good (y/n/r/x/k /u(p)/d(own))? ', print=True, newline=True)
                    # k = readcharacter(msg='Good (y/n/r/x/k/s /u(p)/d(own))? ', print=True, newline=True)
                if k in ['x']:
                    break
                elif k in ['u']:
                    i=i-2
                    continue
                elif k in ['d']:
                    if i>=imaxsofar:
                        fd(f'Error: cannot go further down than already was')
                        i=i-1 # To ensure we're not further than we were, at beginning of next round; we must repeat this until we get a valid  behavior
                    continue
                elif k in ['y', ' ', 'k']:
                    donesomething()
                    if k in ['k']: rowcopy['failed_' + m] = False
                    if not rowcopy['failed_' + m]:  # Here could condition on 'not failed' or on 'not redo', both should be fine I think
                        rowcopy['stage_' + m] = rowcopy['stage_' + m] + 1
                    #row['failed_' + m] = False NOW DO NOT RESET: FAILING ONCE IN A TRAINING, MEANS WE'LL SET STAGE=0 ALWAYS
                    rowcopy['redo_' + m] = False
                elif k in ['n','r']:
                    donesomething()
                    rowcopy['failed_' + m] = True
                    if k=='r':
                        rowcopy['redo_' + m] = True
                    else:
                        rowcopy['redo_' + m] = False
                else:
                    fd('Unexpected entry.')
                rowcopy['date_' + m] = now

                s0=redocount()
                ####### COPY BACK rowcopy -> df #########
                df.iloc[idx] = rowcopy.copy()
                #########################################
                s1=redocount()
                fdtmp(f'redos {s0} {s1}')

                # Maybe remind controls
                counter += 1
                if freqshowcontrol and not counter//freqshowcontrol:
                    showcontrols()

                i=i+1

            if k in ['x']:
                fd('Aborted')
                break
        first = False
        # redocount = sum([df['redo_'+m].sum() for m in modes])
        if redocount(): fd(f'Redocount: {redocount()}')
        alldone = (redocount()==0) or (k in ['x'])

    if donesomething_:
        k=''
        while not k in ['y','n','x']:
            k = readcharacter(msg='Store status? (y/n) ', print=True, newline=True)
            if k != 'y':
                k2 = readcharacter(msg='Really abandon all changes? (y/n) ', print=True, newline=True)
                if k2 != 'y':
                    k='' # Force to re-ask whether Store
        if k=='y':
            ok = backup(fn)
            if not ok:
                k=''
                while k not in ['k', 'q']:
                    k = readcharacter(msg=f'Backup of original file, {fn} failed. Please manually backup (if you want) and '
                                          f'then press k to continue saving file, or q to abandon all changes ',
                                      print=True, newline=True)
                    ok = k=='k'
            if ok:
                if True:
                    fn_preclean = fn+'_preclean.xlsx'
                    try:
                        df.to_excel(fn_preclean, index=False)
                    except Exception as e:
                        fd("Failed to write pre-clean file {}".format(fn_preclean))
                clean_df(df)
                fn_preclean = fn + '_preclean.xlsx'
                fn_use = fn
                ok = True
                try:
                    df.to_excel(fn, index=False)
                except Exception as e:
                    fd('Unable to write to {}. Is it open in excel?'.format(fn))
                    ok = False
                    i = 0
                    while i<100 and not ok:
                        try:
                            fn_use = fn+'_{}.xlsx'.format(i)
                            df.to_excel(fn_use, index=False)
                            ok = True
                        except Exception as e2:
                            pass
                        i += 1
                if ok:
                    fd('Written update to {}'.format(fn_use))
                else:
                    fd("Failed to write file {} or alternatives up to {}".format(fn,fn_use))


