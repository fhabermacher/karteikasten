import numpy as np # just to be able to plot nicely, easily
import pathlib

def f(a,msg=""): assert (a),"Error: "+(msg); return True
def none(a,msg=""): assert (a) is None, "Expected None but isn't: "+(msg); return True
def notnone(a,msg=""): assert (a) is not None, "None where shouldn't: "+(msg); return True
def eq(a,b,msg=""): assert (a)==(b), str(a)+"!="+str(b)+": "+(msg); return True
def eqtol(a,b,tol,msg=""): assert abs((a)-(b))<=(tol), str(a)+" != "+str(b)+"+/-"+str(tol)+" "+(msg); return True
def eq0(a,msg=""): assert (a)==0, str(a)+" != 0: "+(msg); return True
def eq0tol(a,tol,msg=""): assert abs(a)<(tol), str(a)+" != 0+-"+str(tol)+": "+(msg); return True
def arrayeq(a,b,msg=""): assert np.array_equal((a),(b)), "Arrays not equal:\n"+str(a)+"!="+str(b)+": "+(msg); return True
def g0(a,msg=""): assert (a)>0, str(a)+"<=0: "+(msg); return True
def ge0(a,msg=""): assert (a)>=0, str(a)+"<0: "+(msg); return True
def ge(a,b,msg=""): assert (a)>=(b), str(a)+"<"+str(b)+": "+(msg); return True
def g(a,b,msg=""): assert (a)>(b), str(a)+"<="+str(b)+": "+(msg); return True
def l(a,b,msg=""): assert (a)<(b), str(a)+">="+str(b)+": "+(msg); return True
def rng(a,mi,ma,msg=""): assert (a)>=(mi) and (a)<=(ma), str(a)+"not in range ["+str(mi)+".."+str(ma)+"]: "+(msg); return True
def rngtol(a,mi,ma,tol,msg=""): assert (a)>=(mi-tol) and (a)<=(ma+tol), str(a)+"not in range ["+str(mi)+".."+str(ma)+"] even with tol {}: ".format(tol)+(msg); return True
def le(a,b,msg=""): assert (a)<=(b), str(a)+">"+str(b)+": "+(msg); return True
def ltol(a,b,tol,msg=""): assert (a)<=(b)+(tol), str(a)+">"+str(b)+" by more than tol "+str(tol)+" ("+str(b)+str(tol)+"): "+(msg); return True
def finite(a,msg=""): assert not np.isnan(a), str(a)+" not finite: "+(msg); return True
def length(a,len_,msg=""): assert len((a)) == (len_), str(a)+" Length not "+str((len_))+':\n'+(a); return True
def isin(a,list,msg=""): assert (a) in (list), str(a)+" not in list: "+(msg)+": "+str(list); return True
def arein(a_s,list,msg=""):
    missing = [a for a in a_s if not (a in list)]
    assert not missing, "Each of\n    "+str(missing)+" (from original "+str(a_s)+")\nnot in list\n    "+str(list)+":\n"+(msg); return True # Whether list (or so) contains each element of a_s
def file(a,msg=''): path = pathlib.Path((a)); assert path.exists() and path.is_file(), str(a)+': File does not exist'
def path(a,msg=''): path = pathlib.Path((a)); assert path.exists() and not path.is_file(), str(a)+': Path does not exist (at least not strictly as path)'
def fileorpath(a,msg=''): path = pathlib.Path((a)); assert path.exists(), str(a)+': Neither a path nor a file'
